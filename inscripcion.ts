import { Component, OnInit } from '@angular/core';
import { Inscripcion } from '../../models/inscripcion';
import { Cliente } from '../../models/cliente';
import { AngularFirestore } from '@angular/fire/firestore';
import { Precio } from '../../models/precio';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.scss']
})
export class InscripcionComponent implements OnInit {

  inscripcion: Inscripcion = new Inscripcion();
  clienteSeleccionado: Cliente = new Cliente();
  precios: Precio[] = new Array<Precio>();
  idPrecio = 'null';
  precioSeleccionado: Precio = new Precio();

  constructor(private db: AngularFirestore) { }

  ngOnInit(): void {
    this.db.collection('precios').get().subscribe(result =>{
       result.docs.forEach(item =>{
        let precio = item.data() as Precio;
        precio.id = item.id;
        precio.ref = item.ref;
        this.precios.push(precio);
       });
       console.log(this.precios);
    });
  }

  asignarCliente(cliente: Cliente){
    this.inscripcion.cliente = cliente.ref;
    this.clienteSeleccionado = cliente;
  }

  eliminarCliente(){
    this.clienteSeleccionado = new Cliente(); /// Eliminando al Cliente
    this.inscripcion.cliente = undefined;
  }

  seleccionoPrecio(id: string){

    if (id !== 'null'){
      /// compara el id con el id del arreglo precios que esta extrayendo informacion de firebase
      this.precioSeleccionado = this.precios.find(x => x.id === id);
      this.inscripcion.precios = this.precioSeleccionado.ref; /// TODO Asignada los valores al campo precios de inscripcion
      this.inscripcion.fechaInicio = new Date(); // TODO asignando la fecha de inicio de acuerdo a la hora del sistema


      if (this.precioSeleccionado.tipoDuracion === '1'){

        const dias: number = this.precioSeleccionado.duracion;
        let fechaFinal = new Date(this.inscripcion.fechaInicio.getFullYear(),
                                  this.inscripcion.fechaInicio.getMonth(),
                                  this.inscripcion.fechaInicio.getDate() + dias);
        console.log(fechaFinal);
        this.inscripcion.fechaFinal = fechaFinal;
      }
      if (this.precioSeleccionado.tipoDuracion === '2'){

        const dias: number = this.precioSeleccionado.duracion * 7;
        console.log(dias);
        let fechaFinal = new Date(this.inscripcion.fechaInicio.getFullYear(),
                                  this.inscripcion.fechaInicio.getMonth(),
                                  this.inscripcion.fechaInicio.getDate() + dias);
        console.log(fechaFinal);
        this.inscripcion.fechaFinal = fechaFinal;
      }
      if (this.precioSeleccionado.tipoDuracion === '3'){

        const dias: number = this.precioSeleccionado.duracion * 15;
        console.log(dias);
        let fechaFinal = new Date(this.inscripcion.fechaInicio.getFullYear(),
                                  this.inscripcion.fechaInicio.getMonth(),
                                  this.inscripcion.fechaInicio.getDate() + dias);
        console.log(fechaFinal);
        this.inscripcion.fechaFinal = fechaFinal;
      }
      if (this.precioSeleccionado.tipoDuracion === '4'){

        let meses: number = this.precioSeleccionado.duracion;

        let fechaFinal = new Date(this.inscripcion.fechaInicio.getFullYear(),
                                  this.inscripcion.fechaInicio.getMonth() + meses,
                                  this.inscripcion.fechaInicio.getDate());
        console.log(fechaFinal);
        this.inscripcion.fechaFinal = fechaFinal;
      }
      if (this.precioSeleccionado.tipoDuracion === '5'){
        let anio: number = this.precioSeleccionado.duracion;

        let fechaFinal = new Date(this.inscripcion.fechaInicio.getFullYear() + anio,
                                  this.inscripcion.fechaInicio.getMonth(),
                                  this.inscripcion.fechaInicio.getDate());
        console.log(fechaFinal);
        this.inscripcion.fechaFinal = fechaFinal;
      }

      this.inscripcion.subtotal = this.precioSeleccionado.costo;
      this.inscripcion.iva = this.inscripcion.subtotal * 0.15;
      this.inscripcion.total = this.inscripcion.subtotal + this.inscripcion.iva;

    // console.log(this.precioSeleccionado);
    }
    else{
      this.inscripcion.precios = null;
      this.inscripcion.fechaInicio = null;
      this.inscripcion.fechaFinal = null;
      this.precioSeleccionado = new Precio();
      this.inscripcion.subtotal = 0;
      this.inscripcion.iva = 0;
      this.inscripcion.total = 0;
    }
  }

  guardarInscripcion(){

    ///TODO angular solo guarda objeto

    if (this.inscripcion.validar().esValido){
      let inscripcionAgregar = {
        fechaInicio: this.inscripcion.fechaInicio,
        fechaFinal: this.inscripcion.fechaFinal,
        cliente: this.inscripcion.cliente, /// Referencia a la coleccion del Clientes
        precios: this.inscripcion.precios, /// Referencia a la coleccion del Precios
        subtotal: this.inscripcion.subtotal,
        iva: this.inscripcion.iva,
        total: this.inscripcion.total
      }

      this.db.collection('inscripciones').add(inscripcionAgregar)
             .then((result) => {
              Swal.fire({
                title: 'Registro Exitoso',
                text: 'Inscripcion Guardada con exito',
                icon: 'success'
              });
              this.inscripcion = new Inscripcion();
              this.clienteSeleccionado = new Cliente();
              this.precioSeleccionado = new Precio();
              this.idPrecio = 'null';
             });
    }
    else{
      Swal.fire({
        title: 'Advertencia',
        text: `${this.inscripcion.validar().mensaje}`,
        icon: 'info'
      });
    }
  }
}
