import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../../services/firestore.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-gatos',
  templateUrl: './gatos.component.html',
  styleUrls: ['./gatos.component.css']
})
export class GatosComponent implements OnInit {

  public cats = [];
  public documentId = null;
  public currentStatus = 1;

  public newCatForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    url: new FormControl('', Validators.required),
    id: new FormControl('')
  });
  constructor(private firestoreServicio: FirestoreService) {
    this.newCatForm.setValue({
      id: '',
      nombre: '',
      url: '',
    });
  }

  ngOnInit(): void {
    this.firestoreServicio.getCats().subscribe((data) => {
      this.cats = [];
      data.docs.forEach((item: any) => {
        this.cats.push({
          id: item.id,
          data: item.data()
        });
        // console.log(this.cats);
      });
    });
  }

  public newCat(form, documentId = this.documentId){
    // console.log(`Status: ${this.currentStatus}`);
    if (this.currentStatus === 1) {
      let data = {
        nombre: form.nombre,
        url: form.url
      }
      this.firestoreServicio.createCat(data).then(() => {
        // console.log('Documento creado exitósamente!');
        this.newCatForm.setValue({
          nombre: '',
          url: '',
          id: ''
        });
      }, (error) => {
        console.error(error);
      });
    } else {
      let data = {
        nombre: form.nombre,
        url: form.url
      }
      this.firestoreServicio.updateCat(documentId, data).then(() => {
        this.currentStatus = 1;
        this.newCatForm.setValue({
          nombre: '',
          url: '',
          id: ''
        });
        // console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }
  }

  public editCat(documentId) {
    let editSubscribe = this.firestoreServicio.getCat(documentId).subscribe((cat) => {
      this.currentStatus = 2;
      this.documentId = documentId;
      this.newCatForm.setValue({
        id: documentId,
        nombre: cat.()['nombre'],
        url: cat.()['url']
      });
      editSubscribe.unsubscribe();
    });
  }

  public deleteCat(documentId) {
    this.firestoreServicio.deleteCat(documentId).then(() => {
      // console.log('Documento eliminado!');
    }, (error) => {
      console.error(error);
    });
  }

}
