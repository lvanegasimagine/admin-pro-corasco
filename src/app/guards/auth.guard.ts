import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { map, take, tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private afsAuth: AngularFireAuth, private router: Router) {

  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.afsAuth.authState.pipe(take(1)).pipe(map(authState => !!authState)).pipe(tap(auth => {
      if (!auth) {
        this.router.navigateByUrl('/login');
      }
    }));
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.afsAuth.authState.pipe(take(1)).pipe(map(authState => !!authState)).pipe(tap(auth => {
      if (!auth) {
        this.router.navigateByUrl('/login');
      }
    }));
  }

}
