import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'

import { ChartsModule } from 'ng2-charts';

import { IncrementadorComponent } from './incrementador/incrementador.component';
import { DonaComponent } from './dona/dona.component';
import { MensajeComponent } from './mensaje/mensaje.component';



@NgModule({
  declarations: [
    IncrementadorComponent,
    DonaComponent,
    MensajeComponent
  ],
  exports: [
    IncrementadorComponent,
    DonaComponent,
    MensajeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ]
})
export class ComponentsModule { }
