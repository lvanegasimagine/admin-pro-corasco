import { Component, OnInit } from '@angular/core';
import { SidebarService } from 'src/app/services/sidebar.service';

@Component({
  selector: 'app-mensaje',
  templateUrl: './mensaje.component.html',
  styleUrls: ['./mensaje.component.css']
})
export class MensajeComponent implements OnInit {

  public listarContacto = [];
  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit(): void {
    this.cargarContacto();
  }

  cargarContacto() {
    this.sidebarService.getsContacto().subscribe((data) => {
      this.listarContacto = [];
      data.forEach((resp: any) => {
        this.listarContacto.push({
          id: resp.payload.doc.id,
          data: resp.payload.doc.data()
        });
      });
      console.log(this.listarContacto);
    });
  }

}
