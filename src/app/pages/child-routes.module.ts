import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ListadoClienteComponent } from './listado-cliente/listado-cliente.component';
import { RouterModule, Routes } from '@angular/router';
import { ProyectoComponent } from './proyecto/proyecto.component';
import { ListadoProyectoComponent } from './listado-proyecto/listado-proyecto.component';
import { ListadoGaleriaComponent } from './listado-galeria/listado-galeria.component';
import { InformacionPageComponent } from './informacion-page/informacion-page.component';
import { ListadoProyectoMtiComponent } from './proyectos-MTI/listado-proyecto-mti/listado-proyecto-mti.component';
import { ListadoCategoriaComponent } from './listado-categoria/listado-categoria.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { ProyectoMTIComponent } from './proyectos-MTI/proyecto-mti/proyecto-mti.component';

const childRoutes: Routes = [
  { path: '', component: DashboardComponent, data: { titulo: 'Dashboard' } },

  { path: 'Informacion-Web', component: InformacionPageComponent, data: { titulo: 'Información Pagina' } },

  //Categoria
  { path: 'listado-categoria', component: ListadoCategoriaComponent, data: { titulo: 'Listado-Categoria' } },
  { path: 'categoria/nuevo', component: CategoriaComponent, data: { titulo: 'Crear Categoria' } },
  { path: 'categoria/:categoriaId', component: CategoriaComponent, data: { titulo: 'Actualizar Categoria' } },


  { path: 'cliente/nuevo', component: ClienteComponent, data: { titulo: 'Cliente' } },
  { path: 'cliente/:clienteId', component: ClienteComponent, data: { titulo: 'Actualizar Cliente' } },
  { path: 'listado-cliente', component: ListadoClienteComponent, data: { titulo: 'Listado-Cliente' } },

  { path: 'proyecto/nuevo', component: ProyectoComponent, data: { titulo: 'Agregar Proyecto' } },
  { path: 'proyecto/:proyectoId', component: ProyectoComponent, data: { titulo: 'Actualizar Proyecto' } },
  { path: 'listado-proyecto', component: ListadoProyectoComponent, data: { titulo: 'Listado-Proyecto' } },

  // Proyectos MTI

  { path: 'listado-proyecto-mti', component: ListadoProyectoMtiComponent, data: { titulo: 'Listar Proyecto MTI' } },
  { path: 'proyectosMTI/nuevo', component: ProyectoMTIComponent, data: { titulo: 'Nuevo Proyecto MTI' } },


  { path: 'galeria', component: GaleriaComponent, data: { titulo: 'Galeria'}},
  { path: 'galeria/nuevo', component: GaleriaComponent, data: { titulo: 'Agregar Galeria' } },
  { path: 'galeria/:id', component: GaleriaComponent, data: { titulo: 'Galeria+1'}},
  // { path: 'galeria/:id', component: GaleriaComponent, data: { titulo: 'Actualizar Galeria' } },
  { path: 'listado-galeria', component: ListadoGaleriaComponent, data: {titulo: 'Listado-Galeria'} },

  { path: 'progress', component: ProgressComponent, data: { titulo: 'ProgressBar' } },
  { path: 'grafica1', component: Grafica1Component, data: { titulo: 'Gráfica #1' }},
  { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Ajustes de cuenta' }},
  { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas' }},
  { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJs' }},
]
@NgModule({
  imports: [ RouterModule.forChild(childRoutes) ],
  exports: [ RouterModule ]
})
export class ChildRoutesModule { }
