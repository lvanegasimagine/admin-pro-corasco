import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import Swal from 'sweetalert2';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  public clienteForm: FormGroup;
  esEditable = false;
  urlImg = '';
  id = '';
  progreso = 0;
  public imagenSubir: File;
  // retoma la img previa a la actualizacion
  public imgTemp: any = null;

  constructor(private router: Router, private activeRoute: ActivatedRoute,
              private fb: FormBuilder, private storage: AngularFireStorage, private db: AngularFirestore)
  {

  }

  ngOnInit(): void {
    this.formulario();
    this.id = this.activeRoute.snapshot.params.clienteId;

    if (this.id !== undefined) {
      this.esEditable = true; /// su funcion es ocultar el boton agregar cuando dicha variable sea falsa
      this.db.doc<any>(`clientes/${this.id}`).valueChanges().subscribe(result => {
        this.clienteForm.setValue({
          nombre: result.nombre,
          email: result.email,
          ruc: result.ruc,
          telefono: result.telefono,
          siglas: result.siglas,
          sector: result.sector,
          imgUrl: ''
        });
        this.urlImg = result.imgUrl;
    });
    }
  }

  formulario() {
    this.clienteForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      ruc: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      siglas: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      sector: ['publico', [Validators.required]],
      imgUrl: [''],
    });
  }

  agregarCliente() {
    this.clienteForm.value.imgUrl = this.urlImg;

    this.db.collection('clientes').add(this.clienteForm.value).then((listo) => {
      Swal.fire({
        title: 'Registro Exitoso',
        text: '',
        icon: 'success'
      });
      this.clienteForm.reset();
      this.progreso = 0;
    });
    this.router.navigateByUrl('/dashboard/listado-cliente');
  }

  actualizarCliente() {
    console.log(this.clienteForm.value.imgUrl);
    this.clienteForm.value.imgUrl = this.urlImg;
    console.log(this.urlImg);


    this.db.doc('clientes/' + this.id).update(this.clienteForm.value)
                                      .then((resultado) => {
                                        Swal.fire({
                                          title: 'Actualización',
                                          text: 'Datos Actualizado Exitosamente',
                                          icon: 'success'
                                        });
                                      })
                                      .catch((err) => {
                                        Swal.fire({
                                          title: 'Oops Error',
                                          text:  `${err}`,
                                          icon:  'error'
                                        });
                                      });
    this.clienteForm.reset();
    this.router.navigateByUrl('/dashboard/listado-cliente');
  }

  cambiarImagen(file: File) {
    this.imagenSubir = file;

    if (!file) { return this.imgTemp = null; }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    };
  }

  cancelar() {
    this.router.navigateByUrl('/dashboard/listado-cliente');
    this.clienteForm.reset();
  }

  subirImagen(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `clientes/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

  get nombreNoValido(){
    return this.clienteForm.get('nombre').invalid && this.clienteForm.get('nombre').touched;
  }

  get nombreValido(){
    return this.clienteForm.get('nombre').valid;
  }

  get emailNoValido(){
    return this.clienteForm.get('email').invalid && this.clienteForm.get('email').touched;
  }

  get emailValido(){
    return this.clienteForm.get('email').valid;
  }

  get rucNoValido(){
    return this.clienteForm.get('ruc').invalid && this.clienteForm.get('ruc').touched;
  }

  get rucValido(){
    return this.clienteForm.get('ruc').valid;
  }

  get siglasNoValido(){
    return this.clienteForm.get('siglas').invalid && this.clienteForm.get('siglas').touched;
  }

  get siglasValido(){
    return this.clienteForm.get('siglas').valid;
  }

  get telefonoNoValido(){
    return this.clienteForm.get('telefono').invalid && this.clienteForm.get('telefono').touched;
  }

  get telefonoValido(){
    return this.clienteForm.get('telefono').valid;
  }

}
