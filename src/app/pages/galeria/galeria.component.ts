import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientesService } from '../../services/clientes.service';
import { ProyectoService } from '../../services/proyecto.service';
import { UploadService } from '../../services/upload.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GaleriaService } from '../../services/galeria.service';
import Swal from 'sweetalert2';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent implements OnInit {

  public galeriaForm: FormGroup;
  public clientes: any[];
  esEditable = false;
  public proyectos: any[];
  public Foto_1 = '';
  public Foto_2 = '';
  public Foto_3 = '';
  public Foto_4 = '';
  public Foto_5 = '';
  public Foto_6 = '';
  public imgTemp: any = null;
  progreso = 0;
  public imagenSubir: File;
  id= '';
  // retoma la img previa a la actualizacion

  constructor(private activatedRoute:ActivatedRoute, private db: AngularFirestore, private router:Router, private fb: FormBuilder, public uploadService: UploadService, private storage: AngularFireStorage, private galeriaService:GaleriaService, private proyectoService:ProyectoService, private clientesService:ClientesService) { }

  ngOnInit(): void {
    this.galeriaFormulario();
    this.cargarClientes();
    this.cargarProyectos();
    this.id = this.activatedRoute.snapshot.params.id;
    console.log(this.id);

    if (this.id !== undefined && this.id !== 'nuevo')
    {
      this.esEditable = true; /// su funcion es ocultar el boton agregar cuando dicha variable sea falsa
      this.db.doc<any>(`galeria/${this.id}`).valueChanges().subscribe((result: any) => {
        this.galeriaForm.setValue({
          nombreProyecto: result.nombreProyecto,
          nombreCliente: result.nombreCliente,
          Foto_1: '',
          Foto_2: '',
          Foto_3: '',
          Foto_4: '',
          Foto_5: '',
          Foto_6: '',
        });
        this.Foto_1 = result.Foto_1;
        this.Foto_2 = result.Foto_2;
        this.Foto_3 = result.Foto_3;
        this.Foto_4 = result.Foto_4;
        this.Foto_5 = result.Foto_5;
        this.Foto_6 = result.Foto_6;
      });
    }
  }



  galeriaFormulario() {
    this.galeriaForm = this.fb.group({
      nombreProyecto: ['', Validators.required],
      nombreCliente: ['', Validators.required],
      Foto_1: ['', [Validators.required]],
      Foto_2: ['', [Validators.required]],
      Foto_3: ['', [Validators.required]],
      Foto_4: ['', [Validators.required]],
      Foto_5: ['', [Validators.required]],
      Foto_6: ['', [Validators.required]],
    });
  }

  cargarProyectos() {
    this.proyectoService.getProyectosMTI().subscribe((resp: any) => {
      this.proyectos = [];
      resp.forEach((data: any) => {
        console.log(data);
        this.proyectos.push({
          id: data.payload.doc.id,
          data: data.payload.doc.data()
        })
      });
      console.log(this.proyectos);
    })
  }

  cargarClientes() {
    this.clientesService.getClientes().subscribe((resp: any) => {
      this.clientes = []
      resp.forEach((data: any) => {
        this.clientes.push({
          id: data.payload.doc.id,
          data: data.payload.doc.data()
        })
        console.log(this.clientes);
      });
    })
  }

  crearGaleria(form: any) {
    this.galeriaForm.value.Foto_1 = this.uploadService.urlImg_1;
    this.galeriaForm.value.Foto_2 = this.uploadService.urlImg_2;
    this.galeriaForm.value.Foto_3 = this.uploadService.urlImg_3;
    this.galeriaForm.value.Foto_4 = this.uploadService.urlImg_4;
    this.galeriaForm.value.Foto_5 = this.uploadService.urlImg_5;
    this.galeriaForm.value.Foto_6 = this.uploadService.urlImg_6;

    this.galeriaService.createGaleria(form).then((resp) => {
      Swal.fire('Exito', 'Galeria Creada', 'success');
      this.router.navigateByUrl('dashboard/listado-galeria')
    }).catch((error) => {
      Swal.fire('Error', 'Comunicate con el Administrador', 'error');
    });
  }

  actualizarGaleria() {
    this.galeriaForm.value.Foto_1 = this.Foto_1;
    // this.uploadService.urlImg_1;
    this.galeriaForm.value.Foto_2 = this.Foto_2;
    // this.uploadService.urlImg_2;
    this.galeriaForm.value.Foto_3 = this.Foto_3;
    // this.uploadService.urlImg_3;
    this.galeriaForm.value.Foto_4 = this.Foto_4;
    // this.uploadService.urlImg_4;
    this.galeriaForm.value.Foto_5 = this.Foto_5;
    // this.uploadService.urlImg_5;
    this.galeriaForm.value.Foto_6 = this.Foto_6;
    // this.uploadService.urlImg_6;


    this.db.doc('galeria/' + this.id).update(this.galeriaForm.value)
                                      .then((resultado) => {
                                        Swal.fire({
                                          title: 'Actualización',
                                          text: 'Datos Actualizado Exitosamente',
                                          icon: 'success'
                                        });
                                      })
                                      .catch((err) => {
                                        Swal.fire({
                                          title: 'Oops Error',
                                          text:  `${err}`,
                                          icon:  'error'
                                        });
                                      });
    this.galeriaForm.reset();
    this.router.navigateByUrl('/dashboard/listado-galeria');
  }

  cambiarImagen(file: File) {
    this.imagenSubir = file;

    if (!file) { return this.imgTemp = null; }

    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      this.imgTemp = reader.result;
    };
  }

  subirImagen1(event){
    this.uploadService.subirImagen_1(event);
  }
  subirImagen2(event){
    this.uploadService.subirImagen_2(event);
  }
  subirImagen3(event){
    this.uploadService.subirImagen_3(event);
  }
  subirImagen4(event){
    this.uploadService.subirImagen_4(event);
  }
  subirImagen5(event){
    this.uploadService.subirImagen_5(event);
  }
  subirImagen6(event){
    this.uploadService.subirImagen_6(event);
  }

  cancelar() {
    this.router.navigateByUrl('/dashboard/listado-galeria');
  }
}
