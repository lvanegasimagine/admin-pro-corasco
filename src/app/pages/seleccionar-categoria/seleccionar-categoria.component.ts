import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Categoria } from "../../models/categoria.model";
@Component({
  selector: 'app-seleccionar-categoria',
  templateUrl: './seleccionar-categoria.component.html',
  styleUrls: ['./seleccionar-categoria.component.css']
})
export class SeleccionarCategoriaComponent implements OnInit {

  categorias: Categoria[] = new Array<Categoria>();
  @Input('nombre') nombre: string;
  @Output('seleccionoCategoria') seleccionoCategoria = new EventEmitter();
  @Output('canceloCategoria') canceloCategoria = new EventEmitter();

  constructor(private db: AngularFirestore) { }

  ngOnInit(): void {
    this.db.collection('categoria').get().subscribe((result) => {
      this.categorias.length = 0;
      result.docs.forEach((item) => {

        let categoria: any = item.data(); /// Aca si se coloca el tipo Cliente trae toda la data menos el id
        categoria.id = item.id;
        categoria.ref = item.ref;
        categoria.visible = false;
        this.categorias.push(categoria);
      });
      console.log(this.categorias);
    });
  }

  buscarCategorias(nombre: string){
    this.categorias.forEach((categoria) =>{
      if (categoria.nombre.toLowerCase().includes(nombre.toLowerCase())){
        categoria.visible = true;
      }
      else{
        categoria.visible = false;
      }
    });
  }

  seleccionarCategoria(categoria: Categoria){
    this.nombre = categoria.nombre;
    this.categorias.forEach((categoria) =>{
      categoria.visible = false;
    });

    this.seleccionoCategoria.emit(categoria);
  }

  cancelarCategoria(){
    this.nombre = undefined;
    this.canceloCategoria.emit();
  }

}
