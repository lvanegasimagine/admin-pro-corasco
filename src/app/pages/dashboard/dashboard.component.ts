import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SettingsService, valorReloj } from '../../services/settings.service';
import { ClientesService } from '../../services/clientes.service';
import { ProyectoService } from '../../services/proyecto.service';
import { GaleriaService } from '../../services/galeria.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  datos$: Observable<valorReloj>;
  hora: number;
  minutos: string;
  dia: string;
  fecha: string;
  ampm: string;
  segundos: string;
  public clientes = [];
  public proyecto = [];
  public galeria = [];

  constructor(private settingsService: SettingsService, private clientesService: ClientesService, private proyectoService: ProyectoService, private galeriaService: GaleriaService) { }

  ngOnInit(): void {
    this.datos$ = this.settingsService.getInfoReloj();
    this.datos$.subscribe(x => {
      this.hora = x.hora;
      this.minutos = x.minutos;
      this.dia = x.diadesemana;
      this.fecha = x.diaymes;
      this.ampm = x.ampm;
      this.segundos = x.segundo
    });
    this.totalCollection();
  }

  totalCollection(){
      this.clientesService.getClientes().subscribe((data) => {
        this.clientes = [];
        data.forEach((resp: any) => {
          this.clientes.push({
            id: resp.payload.doc.id,
            data: resp.payload.doc.data()
          });
        });
      });
      this.proyectoService.getProyectos().subscribe((data) => {
        this.proyecto = [];
        data.forEach((resp: any) => {
          this.proyecto.push({
            id: resp.payload.doc.id,
            data: resp.payload.doc.data()
          });
        });
      });
      this.galeriaService.getGalerias().subscribe((data) => {
        this.galeria = [];
        data.forEach((resp: any) => {
          this.galeria.push({
            id: resp.payload.doc.id,
            data: resp.payload.doc.data()
          });
        });
      });
  }
}
