import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ProyectoService } from '../../services/proyecto.service';

@Component({
  selector: 'app-listado-proyecto',
  templateUrl: './listado-proyecto.component.html',
  styleUrls: []
})
export class ListadoProyectoComponent implements OnInit {

  public listadoProyecto = [];
  public cargando = true;

  constructor(public proyectoService:ProyectoService) { }

  ngOnInit(): void {
    this.cargarProyecto();
  }

  cargarProyecto() {
    this.proyectoService.getProyectos().subscribe((data) => {
      this.listadoProyecto = [];
      data.forEach((resp: any) => {
        this.listadoProyecto.push({
          id: resp.payload.doc.id,
          data: resp.payload.doc.data()
          })
      })
      this.cargando = false;
      console.log(this.listadoProyecto);
    });
  }

  deleteProyecto(id, nombre) {
    Swal.fire({
      title: 'Borrar Proyecto?',
      text: `Esta a punto de borrar a ${nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proyectoService.deleteProyecto(id).then(() => {
          Swal.fire(
            'Eliminado',
            `El proyecto ${nombre} fue eliminado exitosamente`,
            'success'
          );
          this.cargarProyecto();
        });
      }
    },(error) => {
      console.error(error);
    });
  }

}
