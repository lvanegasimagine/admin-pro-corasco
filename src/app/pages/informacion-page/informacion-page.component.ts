import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { SidebarService } from 'src/app/services/sidebar.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'app-informacion-page',
  templateUrl: './informacion-page.component.html',
  styleUrls: ['./informacion-page.component.css']
})
export class InformacionPageComponent implements OnInit {

  public listadoInformacion = [];
  public redesForm: FormGroup;
  public infoGeneralForm: FormGroup;

  constructor(private sidebarService: SidebarService, private fb: FormBuilder, public db: AngularFirestore) { 
    this.formularioRedes();
    this.formularioInfoGeneral();
  }

  formularioRedes() {
    this.redesForm = this.fb.group({
      linkedin: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      facebook: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      instagram: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
    });
  }

  formularioInfoGeneral() {
    this.infoGeneralForm = this.fb.group({
      Direccion: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
      Horario_Semanal: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      Horario_FinSemana: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(150)]],
      Telefono: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
    });
  }

  ngOnInit(): void {
    this.sidebarService.getInformacionGeneral().subscribe((data) => {
      this.listadoInformacion = [];
      data.forEach((resp: any) => {
        this.listadoInformacion.push({
          data: resp.payload.doc.data()
          // id: resp.payload.doc.id,
        });
        console.log(this.listadoInformacion);
      });
    });
  }

  visualizarInformacionGeneral() {
    const id = 'fOCqSSpjXfqa2BZQF2fe';

    this.db.doc<any>(`Pagina-Informacion/${id}`).valueChanges().subscribe(result => {
      this.infoGeneralForm.setValue({
        Direccion: result.Direccion,
        Horario_Semanal: result.Horario_Semanal,
        Horario_FinSemana: result.Horario_FinSemana,
        Telefono: result.Telefono,
        correo: result.correo
      });
    });
  }

  visualizarRedesSociales() {
    const id = 'fOCqSSpjXfqa2BZQF2fe';

    this.db.doc<any>(`Pagina-Informacion/${id}`).valueChanges().subscribe(result => {
      this.redesForm.setValue({
        linkedin: result.linkedin,
        facebook: result.facebook,
        instagram: result.instagram
      });
      // this.urlImg = result.imgUrl;
  });
  }

  actualizarRedes() {
    const id = 'fOCqSSpjXfqa2BZQF2fe';
    this.db.doc('Pagina-Informacion/' + id).update(this.redesForm.value).then((resp: any) => {
      Swal.fire({
        title: 'Redes Sociales',
        text: 'Actualizado Exitosamente',
        icon: 'success'
      });
      $('#redes-modal').modal('hide');
    }).catch((error: any) => {
      Swal.fire({
        title: 'Oops Error',
        text:  `${error}`,
        icon:  'error'
      });
    });
  }

  actualizarInfoGeneral() {
    const id = 'fOCqSSpjXfqa2BZQF2fe';
    this.db.doc('Pagina-Informacion/' + id).update(this.infoGeneralForm.value).then((resp: any) => {
      Swal.fire({
        title: 'Datos Generales',
        text: 'Actualizado Exitosamente',
        icon: 'success'
      });
      $('#infogeneral-modal').modal('hide');
    }).catch((error: any) => {
      Swal.fire({
        title: 'Oops Error',
        text: `${error}`,
        icon: 'error'
      });
    });
  }

  get correoNoValido(){
    return this.infoGeneralForm.get('correo').invalid && this.infoGeneralForm.get('correo').touched;
  }

  get correoValido(){
    return this.infoGeneralForm.get('correo').valid;
  }

}
