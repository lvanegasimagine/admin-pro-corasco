import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { GaleriaService } from '../../services/galeria.service';

@Component({
  selector: 'app-listado-galeria',
  templateUrl: './listado-galeria.component.html',
  styleUrls: ['./listado-galeria.component.css']
})
export class ListadoGaleriaComponent implements OnInit {

  public listadoGaleria = [];
  public cargando: boolean = true;
  constructor(public galeriaService: GaleriaService) { }

  ngOnInit(): void {
    this.cargarGaleria();
  }

  cargarGaleria() {
    this.galeriaService.getGalerias().subscribe((resp) => {
      this.listadoGaleria = [];
      resp.forEach((data) => {
        this.listadoGaleria.push({
          id: data.payload.doc.id,
          data: data.payload.doc.data()
        })
      });
      this.cargando = false;
      console.log(this.listadoGaleria);
    });
  }

  deleteGaleria(documentId: string, nombre: string) {
    Swal.fire({
      title: 'Borrar Galeria?',
      text: `Esta a punto de borrar la galeria de ${nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.galeriaService.deleteGaleria(documentId).then(() => {
          Swal.fire(
            'Eliminado',
            `La galeria ${nombre} fue eliminado exitosamente`,
            'success'
          );
          this.cargarGaleria();
        });
      }
    },(error) => {
      console.error(error);
    });
  }

  galeriaId(id: string) {
    this.galeriaService.id = id;
  }

}
