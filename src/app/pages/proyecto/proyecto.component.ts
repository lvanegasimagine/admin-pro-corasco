import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProyectoService } from '../../services/proyecto.service';
import { ClientesService } from '../../services/clientes.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {

  public proyectoForm: FormGroup;
  esEditable = false;
  public listarCliente = [];
  public changeAE: boolean = true;
  public id = '';

  constructor(public fb: FormBuilder,
    private db: AngularFirestore,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public clientesService: ClientesService,
    public proyectoService: ProyectoService) { }

  ngOnInit(): void {
    this.proyectoFormulario();
    this.cargarClientes();
    this.id = this.activatedRoute.snapshot.params.proyectoId;

    if (this.id !== undefined) {
      this.esEditable = true;
      this.db.doc<any>(`proyectos/${this.id}`).valueChanges().subscribe(result => {
        this.proyectoForm.setValue({
          id_cliente: result.id_cliente,
          nombreProyecto: result.nombre,
          contrato: result.contrato,
          fecha_inicio: new Date(result.fecha_inicio.seconds * 1000).toISOString().substr(0, 10),
          fecha_final: new Date(result.fecha_final.seconds * 1000).toISOString().substr(0, 10),
          finalizado: result.finalizado
        });
      });
    }
  }

  proyectoFormulario() {
    this.proyectoForm = this.fb.group({
      id_cliente: ['', Validators.required],
      nombreProyecto: ['', [Validators.required, Validators.maxLength(250), Validators.minLength(3)]],
      contrato: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(3)]],
      fecha_inicio: ['', Validators.required],
      fecha_final: ['', Validators.required],
      finalizado: [this.changeAE, Validators.required]
    });
  }

  cargarClientes() {
    this.clientesService.getClientes().subscribe((data) => {
      this.listarCliente = [];
      data.forEach((catData: any) => {
        this.listarCliente.push({
          id: catData.payload.doc.id,
          data: catData.payload.doc.data()
        });
      });
    });
  }

  agregarProyecto(form) {
    let data = {
      id_cliente: form.id_cliente,
      nombre: form.nombreProyecto,
      contrato: form.contrato,
      fecha_inicio: new Date(form.fecha_inicio),
      fecha_final: new Date(form.fecha_final),
      finalizado: form.finalizado
    }
    this.proyectoService.createProyecto(data).then(() => {
      Swal.fire({
        title: 'Proyecto',
        text: 'Registro exitoso',
        icon: 'success'
      });
      this.proyectoForm.setValue({
        id_cliente: '',
        nombreProyecto: '',
        contrato: '',
        fecha_inicio: '',
        fecha_final: '',
        finalizado: ''
      });
      this.router.navigateByUrl('/dashboard/listado-proyecto');
    }, (error) => {
      console.error(error);
    });
  }

  actualizarProyecto() {

    let data = {
      id_cliente: this.proyectoForm.value.id_cliente,
      nombre: this.proyectoForm.value.nombreProyecto,
      contrato: this.proyectoForm.value.contrato,
      fecha_inicio: new Date(this.proyectoForm.value.fecha_inicio),
      fecha_final: new Date(this.proyectoForm.value.fecha_final),
      finalizado: this.proyectoForm.value.finalizado
    }

    this.db.doc('proyectos/' + this.id).update(data)
                                      .then((resultado) => {
                                        Swal.fire({
                                          title: 'Actualización',
                                          text: 'Datos Actualizado Exitosamente',
                                          icon: 'success'
                                        });
                                      })
                                      .catch((err) => {
                                        Swal.fire({
                                          title: 'Oops Error',
                                          text:  `${err}`,
                                          icon:  'error'
                                        });
                                      });
    this.proyectoForm.reset();
    this.router.navigateByUrl('/dashboard/listado-proyecto');
    console.log(this.proyectoForm.value);
  }

  cancelar() {
    this.router.navigateByUrl('/dashboard/listado-proyecto');
    this.proyectoForm.reset();
  }

  get nombreNoValido(){
    return this.proyectoForm.get('nombreProyecto').invalid && this.proyectoForm.get('nombreProyecto').touched;
  }

  get nombreValido(){
    return this.proyectoForm.get('nombreProyecto').valid;
  }

  get contratoNoValido(){
    return this.proyectoForm.get('contrato').invalid && this.proyectoForm.get('contrato').touched;
  }

  get contratoValido(){
    return this.proyectoForm.get('contrato').valid;
  }

  get fechaInicioNoValido(){
    return this.proyectoForm.get('fecha_inicio').invalid && this.proyectoForm.get('fecha_inicio').touched;
  }

  get fechaInicioValido(){
    return this.proyectoForm.get('fecha_inicio').valid;
  }

  get fechaFinalNoValido(){
    return this.proyectoForm.get('fecha_final').invalid && this.proyectoForm.get('fecha_final').touched;
  }

  get fechaFinalValido(){
    return this.proyectoForm.get('fecha_final').valid;
  }
}
