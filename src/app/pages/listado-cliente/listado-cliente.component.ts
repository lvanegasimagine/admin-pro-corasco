import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ClientesService } from '../../services/clientes.service';

@Component({
  selector: 'app-listado-cliente',
  templateUrl: './listado-cliente.component.html',
  styles: [
  ]
})
export class ListadoClienteComponent implements OnInit {

  public listarCliente = [];
  cargando: boolean = true;

  constructor(public clientesService:ClientesService) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  cargarCliente() {
    this.clientesService.getClientes().subscribe((data) => {
      this.listarCliente = [];
      data.forEach((resp: any) => {
        this.listarCliente.push({
          id: resp.payload.doc.id,
          data: resp.payload.doc.data()
        });
        this.cargando = false;
        console.log(this.listarCliente);
      });
    });
  }

  deleteCliente(documentId, nombre) {
    Swal.fire({
      title: 'Borrar Médico?',
      text: `Esta a punto de borrar a ${nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo'
    }).then((result) => {
      if (result.isConfirmed) {
        console.log(documentId);
        this.clientesService.deleteCliente(documentId).then(() => {
          Swal.fire(
            'Eliminado',
            `El cliente ${nombre} fue eliminado exitosamente`,
            'success'
          );
          this.cargarCliente();
        });
      }
    },(error) => {
      console.error(error);
    });
  }

}
