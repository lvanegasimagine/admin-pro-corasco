import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/models/categoria.model';
import { Inscripcion } from '../../../models/inscripcion.models';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-proyecto-mti',
  templateUrl: './proyecto-mti.component.html',
  styleUrls: ['./proyecto-mti.component.css']
})
export class ProyectoMTIComponent implements OnInit {

  inscripcion: Inscripcion = new Inscripcion();
  categoriaSeleccionado: Categoria = new Categoria();
  progreso = 0;
  progreso1 = 0;
  progreso2 = 0;
  progreso3 = 0;
  urlImg = '';
  urlImg1 = '';
  urlImg2 = '';
  urlImg3 = '';
  id = '';
  esEditable = false;

  constructor(private db: AngularFirestore, private router: Router,
              private activatedRoute: ActivatedRoute, private storage: AngularFireStorage) { }

  ngOnInit(): void {
  }

  guardarInscripcion() {

    ///TODO angular solo guarda objeto
      console.log(this.inscripcion.nombre);
      let inscripcionAgregar = {
        categoria: this.inscripcion.categoria, /// Referencia a la coleccion del Clientes
        nombre: this.inscripcion.nombre,
        descripcion: this.inscripcion.descripcion,
        fotoPortada: this.urlImg,
        foto_1: this.urlImg1,
        foto_2: this.urlImg2,
        foto_3: this.urlImg3
      }
      console.log(this.urlImg3);

      this.db.collection('proyectosMTI').add(inscripcionAgregar)
             .then((result) => {
              Swal.fire({
                title: 'Registro Exitoso',
                text: 'Inscripcion Guardada con exito',
                icon: 'success'
              });
              this.inscripcion = new Inscripcion();
              this.categoriaSeleccionado = new Categoria();
              this.router.navigateByUrl('/dashboard-listado-proyecto-mti');
             });
  }

  asignarCategoria(categoria: Categoria){
    this.inscripcion.categoria = categoria.ref;
    this.categoriaSeleccionado = categoria;
  }

  eliminarCategoria(){
    this.categoriaSeleccionado = new Categoria(); /// Eliminando al Cliente
    this.inscripcion.categoria = undefined;
  }

  subirImagen(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `proyectosMTI/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

  subirImagen1(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `proyectosMTI/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg1 = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso1 = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

  subirImagen2(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `proyectosMTI/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg2 = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso2 = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

  subirImagen3(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `proyectosMTI/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg3 = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso3 = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

}
