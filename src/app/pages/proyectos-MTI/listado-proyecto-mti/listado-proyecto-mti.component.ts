import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ProyectoService } from 'src/app/services/proyecto.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-listado-proyecto-mti',
  templateUrl: './listado-proyecto-mti.component.html',
  styleUrls: ['./listado-proyecto-mti.component.css']
})
export class ListadoProyectoMtiComponent implements OnInit {

  Inscripciones: any[] = [];
  cargando: boolean = true;

  constructor(private proyectoService: ProyectoService, private db: AngularFirestore) { }

  ngOnInit(): void {
    this.cargarProyecto();
  }

  cargarProyecto() {
    this.Inscripciones.length = 0;

    this.db.collection('proyectosMTI').get().subscribe((result) => {
         this.Inscripciones = [];
         result.forEach((item: any) => {
         let inscripcionObtenida = item.data();
         inscripcionObtenida.id = item.id;

         ///TODO pedir informacion de un reference
         this.db.doc(item.data().categoria.path).get().subscribe((cliente) => {
          //  this.Inscripciones = [];
              inscripcionObtenida.clienteObtenido = cliente.data();
              console.log(inscripcionObtenida.clienteObtenido);
              this.Inscripciones.push(inscripcionObtenida);
              this.cargando = false;
           });
         });
    });
  }

  deleteCliente(documentId, nombre) {
    Swal.fire({
      title: 'Borrar proyecto?',
      text: `Esta a punto de borrar el proyecto ${nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proyectoService.deleteProyectoMTI(documentId).then(() => {
          Swal.fire(
            'Eliminado',
            `El Proyecto ${nombre} fue eliminado exitosamente`,
            'success'
          );
          this.cargarProyecto();
        });
      }
    },(error) => {
      console.error(error);
    });
  }
}