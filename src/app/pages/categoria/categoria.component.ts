import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientesService } from '../../services/clientes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  categoriaForm: FormGroup;
  esEditable = false;
  id = '';

  constructor(private fb: FormBuilder, public router: Router,
              public clientesService: ClientesService, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.categoriaForm = this.fb.group({
      nombre: ['', Validators.required]
    });

    this.id = this.activeRoute.snapshot.params.categoriaId;
    console.log(this.id);

    if (this.id !== undefined) {
         this.esEditable = true; /// su funcion es ocultar el boton agregar cuando dicha variable sea falsa
         this.clientesService.getCategoria(this.id).subscribe((result: any) => {
           console.log(result);
           this.categoriaForm.setValue({
             nombre: result.nombre
           });
         });
    }
  } // Fin de ngOnInit

  agregarCategoria() {
    this.clientesService.createCategoria(this.categoriaForm.value).then((resp: any) => {
      Swal.fire({
        title: 'Categoria Guardada Exitosamente',
        text: '',
        icon: 'success'
      });
      this.router.navigateByUrl('dashboard/listado-categoria');
    }).catch((error: any) => {
      Swal.fire({
        title: 'Error',
        text: `${error} mostrar este mensaje al admin.`,
        icon: 'error'
      });
    });
  }

  actualizarCategoria() {
    console.log(this.categoriaForm.value);
    this.clientesService.updateCategoria(this.id, this.categoriaForm.value).then((resp: any) => {
      Swal.fire({
        title: 'Categoria Actualizada Exitosamente',
        text: '',
        icon: 'success'
      });
      this.router.navigateByUrl('dashboard/listado-categoria');
    }).catch((error: any) => {
      Swal.fire({
        title: 'Error',
        text: `${error} mostrar este mensaje al admin.`,
        icon: 'error'
      });
    })
  }

  cancelar() {
    this.router.navigateByUrl('/dashboard/listado-categoria');
    this.categoriaForm.reset();
  }

  get nombreNoValido(){
    return this.categoriaForm.get('nombre').invalid && this.categoriaForm.get('nombre').touched;
  }

  get nombreValido(){
    return this.categoriaForm.get('nombre').valid;
  }


}


//   categoriaForm: FormGroup;
//   esEditable = false;
//   id = '';

//   constructor(private fb: FormBuilder, public clientesService: ClientesService, private activeRoute: ActivatedRoute) { }

//   ngOnInit(): void {
//     this.categoriaForm = this.fb.group({
//        nombre: ['', Validators.required]
//       });
//   }

//   this.id = this.activeRoute.snapshot.params.categoriaId;

  // if (this.id !== undefined) {
  //     this.esEditable = true; /// su funcion es ocultar el boton agregar cuando dicha variable sea falsa
  //     this.clientesService.getCategoria(this.id).subscribe((result: any) => {
  //       this.categoriaForm.setValue({
  //         nombre: result.nombre,
  //       });
  //     });
  //   }

    // agregarCategoria() {
    
    // }

    // get nombreNoValido(){
    //   return this.categoriaForm.get('nombre').invalid && this.categoriaForm.get('nombre').touched;
    // } 

    // get nombreValido(){
    //   return this.categoriaForm.get('nombre').valid;
    // }