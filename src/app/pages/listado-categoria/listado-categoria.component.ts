import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ClientesService } from '../../services/clientes.service';

@Component({
  selector: 'app-listado-categoria',
  templateUrl: './listado-categoria.component.html',
  styleUrls: ['./listado-categoria.component.css']
})
export class ListadoCategoriaComponent implements OnInit {
  public listarCategoria = [];
  cargando: boolean = true;

  constructor(public clientesService:ClientesService) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  cargarCliente() {
    this.clientesService.getCategorias().subscribe((data) => {
      this.listarCategoria = [];
      data.forEach((resp: any) => {
        this.listarCategoria.push({
          id: resp.payload.doc.id,
          data: resp.payload.doc.data()
        });
        this.cargando = false;
        console.log(this.listarCategoria);
      });
    });
  }

  deleteCliente(documentId, nombre) {
    Swal.fire({
      title: 'Borrar categoria?',
      text: `Esta a punto de borrar la categoria ${nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo'
    }).then((result) => {
      if (result.isConfirmed) {
        this.clientesService.deleteCategoria(documentId).then(() => {
          Swal.fire(
            'Eliminado',
            `La Categoria ${nombre} fue eliminado exitosamente`,
            'success'
          );
          this.cargarCliente();
        });
      }
    },(error) => {
      console.error(error);
    });
  }
}
