import { DocumentReference } from '@angular/fire/firestore';

export class Inscripcion{
    categoria: DocumentReference; /// Referencia a la coleccion del Clientes
    nombre: string;
    descripcion: string;
    fotoPortada: string;
    foto_1: string;
    foto_2: string;
    foto_3: string;

    constructor(){
        this.categoria = this.categoria;
        this.nombre = '';
        this.descripcion = '';
        this.fotoPortada = '';
        this.foto_1 = '';
        this.foto_2 = '';
        this.foto_3 = '';
    }
}