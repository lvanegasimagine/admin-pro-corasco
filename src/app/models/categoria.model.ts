import { DocumentReference } from '@angular/fire/firestore';

export class Categoria{
    id: string;
    nombre: string;
    // apellido: string;
    ref: DocumentReference; /// Siempre va ya que se trabajar con firestore
    visible: boolean;

    constructor(){

    }
}