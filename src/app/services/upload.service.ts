import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UploadService
{
  public urlImg_1 = '';
  public urlImg_2 = '';
  public urlImg_3 = '';
  public urlImg_4 = '';
  public urlImg_5 = '';
  public urlImg_6 = '';
  public progreso_1 = 0;
  public progreso_2 = 0;
  public progreso_3 = 0;
  public progreso_4 = 0;
  public progreso_5 = 0;
  public progreso_6 = 0;
  constructor(private storage: AngularFireStorage) { }

  subirImagen_1(event){
    if (event.target.files.length > 0) {

    const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
    const file = event.target.files[0];
    const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
    const extension = temp.toLowerCase();
    const filePath = `galeria/${nombre}${extension}`;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);

    if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
      task.then(objeto => {
        Swal.fire({
          title: 'Imagen cargada Exitosamente',
          text: '',
          icon: 'info'
        });
        // console.log('Imagen Subida');
        ref.getDownloadURL().subscribe(url => this.urlImg_1 = url);
        console.log(this.urlImg_1);
      });
      task.percentageChanges().subscribe(porcentaje => {
        // tslint:disable-next-line: radix
        this.progreso_1 = parseInt(porcentaje.toString());
      });
    }
    else{
      Swal.fire({
        title: 'Error',
        text: 'Extension del Archivo no permitida',
        icon: 'error'
      });
    }
  }
  }
  subirImagen_2(event){
      const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `galeria/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg_2 = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso_2 = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
  }
  subirImagen_3(event){
    const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
    const file = event.target.files[0];
    const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
    const extension = temp.toLowerCase();
    const filePath = `galeria/${nombre}${extension}`;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);

    if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
      task.then(objeto => {
        Swal.fire({
          title: 'Imagen cargada Exitosamente',
          text: '',
          icon: 'info'
        });
        // console.log('Imagen Subida');
        ref.getDownloadURL().subscribe(url => this.urlImg_3 = url);
      });
      task.percentageChanges().subscribe(porcentaje => {
        // tslint:disable-next-line: radix
        this.progreso_3 = parseInt(porcentaje.toString());
      });
    }
    else{
      Swal.fire({
        title: 'Error',
        text: 'Extension del Archivo no permitida',
        icon: 'error'
      });
    }
  }
  subirImagen_4(event){
    const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
    const file = event.target.files[0];
    const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
    const extension = temp.toLowerCase();
    const filePath = `galeria/${nombre}${extension}`;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);

    if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
      task.then(objeto => {
        Swal.fire({
          title: 'Imagen cargada Exitosamente',
          text: '',
          icon: 'info'
        });
        // console.log('Imagen Subida');
        ref.getDownloadURL().subscribe(url => this.urlImg_4 = url);
      });
      task.percentageChanges().subscribe(porcentaje => {
        // tslint:disable-next-line: radix
        this.progreso_4 = parseInt(porcentaje.toString());
      });
    }
    else{
      Swal.fire({
        title: 'Error',
        text: 'Extension del Archivo no permitida',
        icon: 'error'
      });
    }
  }
  subirImagen_5(event){
  const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
  const file = event.target.files[0];
  const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
  const extension = temp.toLowerCase();
  const filePath = `galeria/${nombre}${extension}`;
  const ref = this.storage.ref(filePath);
  const task = ref.put(file);

  if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
    task.then(objeto => {
      Swal.fire({
        title: 'Imagen cargada Exitosamente',
        text: '',
        icon: 'info'
      });
      // console.log('Imagen Subida');
      ref.getDownloadURL().subscribe(url => this.urlImg_5 = url);
    });
    task.percentageChanges().subscribe(porcentaje => {
      // tslint:disable-next-line: radix
      this.progreso_5 = parseInt(porcentaje.toString());
    });
  }
  else{
    Swal.fire({
      title: 'Error',
      text: 'Extension del Archivo no permitida',
      icon: 'error'
    });
  }
  }
  subirImagen_6(event){
    const nombre = new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
    const file = event.target.files[0];
    const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
    const extension = temp.toLowerCase();
    const filePath = `galeria/${nombre}${extension}`;
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);

    if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
      task.then(objeto => {
        Swal.fire({
          title: 'Imagen cargada Exitosamente',
          text: '',
          icon: 'info'
        });
        // console.log('Imagen Subida');
        ref.getDownloadURL().subscribe(url => this.urlImg_6 = url);
      });
      task.percentageChanges().subscribe(porcentaje => {
        // tslint:disable-next-line: radix
        this.progreso_6 = parseInt(porcentaje.toString());
      });
    }
    else{
      Swal.fire({
        title: 'Error',
        text: 'Extension del Archivo no permitida',
        icon: 'error'
      });
    }
  }
}
