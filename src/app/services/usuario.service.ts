import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  token: string;

  constructor(private auth: AngularFireAuth, private router: Router) { }

  loginUsuario(email: string, password: string) {

    this.auth.signInWithEmailAndPassword(email, password).then((usuario: any) =>
    {
      localStorage.setItem('email', email);
      Swal.fire('Bienvenido', ``, 'success');
      setTimeout(() => {
        this.router.navigateByUrl('/dashboard');
      }, 1000);
    }).catch((error) => {
        Swal.fire({
          title: 'Error',
          text: `${error.message}`,
          icon: 'error'
        });
      });
  }

  logout() {
    this.auth.signOut();
  }

  getUserAuth() {
    return this.auth.authState;
  }
}

