import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

interface Proyecto {
  nombre: string,
  contrato: string,
  fecha_inicio: Date,
  fecha_final: Date,
  finalizado: boolean
}

@Injectable({
  providedIn: 'root'
})
export class ProyectoService {

  proyecto: string = 'proyectos';
  proyectoMTI: string = 'proyectosMTI';

  public Proyecto = [];

  constructor(private firestore: AngularFirestore) { }

  // TODO Crea un nuevo gato
  public createProyecto(data: Proyecto) {
    return this.firestore.collection(`${this.proyecto}`).add(data);
  }
  // TODO Obtiene un gato
  public getProyecto(documentId: string) {
    return this.firestore.collection(`${this.proyecto}`).doc(documentId).snapshotChanges();
  }
  // TODO Obtiene todos los gatos
  public getProyectos() {
    return this.firestore.collection(`${this.proyecto}`).snapshotChanges();
  }
  // TODO Actualiza un gato
  public updateProyecto(documentId: string, data: any) {
    return this.firestore.collection(`${this.proyecto}`).doc(documentId).set(data);
  }

  public deleteProyecto(documentoId: string){
    return this.firestore.collection(`${this.proyecto}`).doc(documentoId).delete();
  }

  // ==> COLECCION PROYECTOS MTI <==

  public getProyectosMTI() {
    return this.firestore.collection(`${this.proyectoMTI}`).snapshotChanges();
  }

  public deleteProyectoMTI(documentoId: string){
    return this.firestore.collection(`${this.proyectoMTI}`).doc(documentoId).delete();
  }
}
