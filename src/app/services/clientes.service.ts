import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  cliente = 'clientes';

  constructor(private db: AngularFirestore) {
  }

  public getCliente(documentId: string) {
    // return this.db.collection(`${this.cliente}`).doc(documentId).snapshotChanges();
    return this.db.collection(`${this.cliente}`).doc(documentId).get();
  }

  getClientes() { return this.db.collection(`${this.cliente}`).snapshotChanges(); }

  public deleteCliente(documentoId: string) { return this.db.collection(`${this.cliente}`).doc(documentoId).delete(); }


  public getCategoria(documentId: string) {
    return this.db.collection('categoria').doc(documentId).valueChanges();
  }

  public getCategorias() {
    return this.db.collection('categoria').snapshotChanges();
  }

  public createCategoria(data: any) {
    return this.db.collection('categoria').add(data);
  }
  // TODO Actualiza un gato
  public updateCategoria(documentId: string, data: any) {
    return this.db.collection('categoria').doc(documentId).update(data);
  }

  public deleteCategoria(documentId: string) {
    return this.db.collection('categoria').doc(documentId).delete();
  }
}
