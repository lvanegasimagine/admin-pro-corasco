import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class GaleriaService {

  galeria: string = 'galeria';
  id: string = '';

  public Galeria = [];

  constructor(private firestore: AngularFirestore) { }

  public createGaleria(data: any) {
    return this.firestore.collection(`${this.galeria}`).add(data);
  }
  // TODO Obtiene un gato
  public getGaleria(documentId: string) {
    return this.firestore.collection(`${this.galeria}`).doc(documentId).snapshotChanges();
  }
  // TODO Obtiene todos los gatos
  public getGalerias() {
    return this.firestore.collection(`${this.galeria}`).snapshotChanges();
  }
  // TODO Actualiza un gato
  public updateGaleria(documentId: string, data: any) {
    return this.firestore.collection(`${this.galeria}`).doc(documentId).set(data);
  }

  public deleteGaleria(documentoId: string){
    return this.firestore.collection(`${this.galeria}`).doc(documentoId).delete();
  }
}
