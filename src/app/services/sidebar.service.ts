import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'Pagina Web',
      icono: 'mdi mdi-web',
      submenu: [
        { titulo: 'main', url: '/' },
        { titulo: 'Informacion-Web', url: 'Informacion-Web' },
        { titulo: 'Cliente', url: 'listado-cliente' },
        { titulo: 'Galeria', url: 'listado-galeria' },
        { titulo: 'Categoria', url: 'listado-categoria' },
        // { titulo: 'Proyecto', url: 'listado-proyecto' },
        // { titulo: 'Gráficas', url: 'grafica1' },
        // { titulo: 'rxjs', url: 'rxjs' },
        // { titulo: 'Promesas', url: 'promesas' },
        // { titulo: 'ProgressBar', url: 'progress' },
      ],
      submenuProyecto: [
        { titulo: 'Proyecto MTI', url: 'listado-proyecto-mti' },
        // { titulo: 'Proyecto ENACAL', url: 'listado-proyecto' },
        // { titulo: 'Proyecto FOMAV', url: 'listado-proyecto' },
        // { titulo: 'Proyecto MHCP', url: 'listado-proyecto' },
      ]
    },
  ];

  contacto: string = 'contacto';
  id: string = '';

  public Contacto = [];

  constructor(private firestore: AngularFirestore) { }
  public getsContacto() {
    return this.firestore.collection(`${this.contacto}`).snapshotChanges();
  }

  getInformacionGeneral() {
    return this.firestore.collection('Pagina-Informacion').snapshotChanges();
  }
}
