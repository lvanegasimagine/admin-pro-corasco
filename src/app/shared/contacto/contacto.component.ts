import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  public listarContacto = [];
  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit(): void {
    this.cargarContacto();
  }

  cargarContacto() {
    this.sidebarService.getsContacto().subscribe((data) => {
      this.listarContacto = [];
      data.forEach((resp: any) => {
        this.listarContacto.push({
          id: resp.payload.doc.id,
          data: resp.payload.doc.data()
        });
        // this.cargando = false;
      });
      console.log(this.listarContacto);
    });
  }

}
