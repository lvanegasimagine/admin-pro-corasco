import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UsuarioService } from 'src/app/services/usuario.service';
import { SidebarService } from '../../services/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit, OnDestroy {

  menuItems: any[];
  public email: string = '';
  myUserSub: Subscription;

  constructor( public sidebarService: SidebarService, public usuarioService: UsuarioService ) {

  }

  getUsuario() {
    this.myUserSub = this.usuarioService.getUserAuth().subscribe((resp: any) => this.email = resp.email);
  }

  ngOnDestroy(): void {
    if (this.myUserSub) {
      this.myUserSub.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.email = '';
    this.menuItems = this.sidebarService.menu;
    this.getUsuario();
  }

}
