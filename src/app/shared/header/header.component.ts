import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit, OnDestroy {

  email: string = '';
  myUserSub: Subscription;

  constructor(public usuarioService: UsuarioService, private router: Router) {
  }


  ngOnInit(): void {
    this.email = '';
    this.myUserSub = this.usuarioService.getUserAuth().subscribe((resp: any) => this.email = resp.email);
  }

  ngOnDestroy(): void {
    if (this.myUserSub) {
      this.myUserSub.unsubscribe();
    }
  }

  logout() {
    this.usuarioService.logout();
    this.router.navigateByUrl('/login');
  }


}
