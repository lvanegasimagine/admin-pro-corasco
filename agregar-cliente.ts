import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-cliente',
  templateUrl: './agregar-cliente.component.html',
  styleUrls: ['./agregar-cliente.component.scss']
})
export class AgregarClienteComponent implements OnInit {

  formularioCliente: FormGroup;
  urlImg = '';
  progreso = 0;
  id = '';
  esEditable = false;

  constructor(private fb: FormBuilder, private storage: AngularFireStorage, private db: AngularFirestore,
              private activeRoute: ActivatedRoute, private router: Router) {

  }

  ngOnInit(): void {
    this.formularioCliente = this.fb.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      correo: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      cedula: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      telefono: ['', Validators.required],
      imgUrl: ['', Validators.required]
    });

    this.id = this.activeRoute.snapshot.params.clienteID;

    // tslint:disable-next-line: comment-format
    if (this.id !== undefined ){ ///TODO Si regresa un ID ejecuta lo que es la actualización
      this.esEditable = true; /// su funcion es ocultar el boton agregar cuando dicha variable sea falsa
      this.db.doc<any>(`clientes/${this.id}`).valueChanges().subscribe(result => {
        console.log(new Date(result.fechaNacimiento.seconds * 1000).toISOString().substr(0, 10));
        console.log(result);
        this.formularioCliente.setValue({
          nombre: result.nombre,
          apellido: result.apellido,
          correo: result.correo,
          fechaNacimiento: new Date(result.fechaNacimiento.seconds * 1000).toISOString().substr(0, 10),
          telefono: result.telefono,
          cedula: result.cedula,
          imgUrl: ''
        });

        this.urlImg = result.imgUrl;
    });
    }
  }

  get nombreNoValido(){
    return this.formularioCliente.get('nombre').invalid && this.formularioCliente.get('nombre').touched;
  }

  get nombreValido(){
    return this.formularioCliente.get('nombre').valid;
  }

  get apellidoNoValido(){
    return this.formularioCliente.get('apellido').invalid && this.formularioCliente.get('apellido').touched;
  }

  get apellidoValido(){
    return this.formularioCliente.get('apellido').valid;
  }

  get correoNoValido(){
    return this.formularioCliente.get('correo').invalid && this.formularioCliente.get('correo').touched;
  }

  get correoValido(){
    return this.formularioCliente.get('correo').valid;
  }

  get telefonoNoValido(){
    return this.formularioCliente.get('telefono').invalid && this.formularioCliente.get('telefono').touched;
  }

  get telefonoValido(){
    return this.formularioCliente.get('telefono').valid;
  }

  get cedulaNoValido(){
    return this.formularioCliente.get('cedula').invalid && this.formularioCliente.get('cedula').touched;
  }

  get cedulaValido(){
    return this.formularioCliente.get('cedula').valid;
  }

  get fechaNoValido(){
    return this.formularioCliente.get('fechaNacimiento').invalid && this.formularioCliente.get('fechaNacimiento').touched;
  }

  get fechaValido(){
    return this.formularioCliente.get('fechaNacimiento').valid;
  }

  agregarCliente(){
    this.formularioCliente.value.imgUrl = this.urlImg;
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);
    this.db.collection('clientes').add(this.formularioCliente.value).then((listo) => {
      Swal.fire({
        title: 'Registro Exitoso',
        text: '',
        icon: 'success'
      });
      this.formularioCliente.reset();
      this.progreso = 0;
    });
  }

  actualizarCliente(){
    this.formularioCliente.value.imgUrl = this.urlImg;
    this.formularioCliente.value.fechaNacimiento = new Date(this.formularioCliente.value.fechaNacimiento);


    this.db.doc('clientes/' + this.id).update(this.formularioCliente.value)
                                      .then((resultado) => {
                                        Swal.fire({
                                          title: 'Actualización',
                                          text: 'Datos Actualizado Exitosamente',
                                          icon: 'success'
                                        });
                                      })
                                      .catch((err) => {
                                        Swal.fire({
                                          title: 'Oops Error',
                                          text:  `${err}`,
                                          icon:  'error'
                                        });
                                      });
    this.formularioCliente.reset();
    this.router.navigateByUrl('/listado-cliente');
  }

  subirImagen(event){

    if (event.target.files.length > 0){
      // tslint:disable-next-line: comment-format
      const nombre =   new Date().getTime().toString(); //Asignar nombre a las imagenes mediante la hora
      const file = event.target.files[0];
      const temp = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      const extension = temp.toLowerCase();
      const filePath = `clientes/${nombre}${extension}`;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);

      if (extension === '.jpg' || extension === '.jpeg' || extension === '.gif' || extension === '.png'){
        task.then(objeto => {
          Swal.fire({
            title: 'Imagen cargada Exitosamente',
            text: '',
            icon: 'info'
          });
          // console.log('Imagen Subida');
          ref.getDownloadURL().subscribe(url => this.urlImg = url);
        });
        task.percentageChanges().subscribe(porcentaje => {
          // tslint:disable-next-line: radix
          this.progreso = parseInt(porcentaje.toString());
        });
      }
      else{
        Swal.fire({
          title: 'Error',
          text: 'Extension del Archivo no permitida',
          icon: 'error'
        });
      }
    }
  }

}
